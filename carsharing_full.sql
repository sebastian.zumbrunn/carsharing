-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 16, 2019 at 09:31 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carsharing`
--
CREATE DATABASE IF NOT EXISTS `carsharing` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `carsharing`;

-- --------------------------------------------------------

--
-- Table structure for table `abo`
--

CREATE TABLE `abo` (
  `aboID` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `preis` double NOT NULL,
  `erstellungsDatum` date NOT NULL,
  `erstellungsZeit` time NOT NULL,
  `relatedAboID` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `abo`
--

INSERT INTO `abo` (`aboID`, `name`, `preis`, `erstellungsDatum`, `erstellungsZeit`, `relatedAboID`) VALUES
(1, 'Jahresabo', 190, '2018-12-03', '00:00:00', NULL),
(2, 'Ermässigtes Jahresabo', 190, '2018-12-03', '00:00:00', NULL),
(3, 'Keines', 0, '2018-12-03', '00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ermässigung`
--

CREATE TABLE `ermässigung` (
  `ermässigungID` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `startdatum` date NOT NULL,
  `endDatum` date DEFAULT NULL,
  `reduktion` double NOT NULL,
  `personID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ermässigung`
--

INSERT INTO `ermässigung` (`ermässigungID`, `name`, `startdatum`, `endDatum`, `reduktion`, `personID`) VALUES
(1, 'Familienrabatt', '2019-01-02', NULL, 50, 1),
(2, 'Familienrabatt', '2019-01-02', NULL, 50, 2);

-- --------------------------------------------------------

--
-- Table structure for table `fahrzeug`
--

CREATE TABLE `fahrzeug` (
  `fahrzeugID` bigint(20) UNSIGNED NOT NULL,
  `jahrgang` int(11) NOT NULL,
  `gefahreneKm` int(11) DEFAULT '0',
  `benzinstand` int(11) DEFAULT '100',
  `zustand` int(11) DEFAULT '5',
  `bemerkung` text COLLATE utf8_unicode_ci,
  `parkplatzID` bigint(20) UNSIGNED NOT NULL,
  `fahrzeugTypeID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fahrzeug`
--

INSERT INTO `fahrzeug` (`fahrzeugID`, `jahrgang`, `gefahreneKm`, `benzinstand`, `zustand`, `bemerkung`, `parkplatzID`, `fahrzeugTypeID`) VALUES
(1, 2010, 100000, 100, 5, 'Fast neu', 1, 1),
(2, 2017, 40000, 100, 5, 'Noch neu', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `fahrzeugtype`
--

CREATE TABLE `fahrzeugtype` (
  `fahrzeugTypeID` bigint(20) UNSIGNED NOT NULL,
  `marke` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `beschreibung` text COLLATE utf8_unicode_ci,
  `maxPersonen` int(11) NOT NULL,
  `maxGepäck` int(11) NOT NULL,
  `schaltung` int(11) NOT NULL,
  `imageUUID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kategorieID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fahrzeugtype`
--

INSERT INTO `fahrzeugtype` (`fahrzeugTypeID`, `marke`, `model`, `beschreibung`, `maxPersonen`, `maxGepäck`, `schaltung`, `imageUUID`, `kategorieID`) VALUES
(1, 'Fiat', 'Panda', 'Ein kleines praktisches Auto. Trotz der Kleine, passen 5 Persoen in den Panda', 5, 2, 0, '49d929cc-cbcf-4c10-a954-764e658a8885', 1),
(2, 'Smart', 'Passion', 'Genau das Richtige für zwei frisch Verliebte, gute Kumpels oder einen Einzelgänger. Dieses Stadtmobiel passt in jede Parklücke. Dank der automitschen Schaltung, fährt es sich auch sehr bequem.', 2, 1, 1, 'c2afc421-f383-4279-88f4-ab54ef4bc4b8', 2);

-- --------------------------------------------------------

--
-- Table structure for table `kategorie`
--

CREATE TABLE `kategorie` (
  `kategorieID` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kategorie`
--

INSERT INTO `kategorie` (`kategorieID`, `name`) VALUES
(1, 'M-Budget'),
(2, 'Micro');

-- --------------------------------------------------------

--
-- Table structure for table `kategoriepreis`
--

CREATE TABLE `kategoriepreis` (
  `kategoriePreisID` bigint(20) UNSIGNED NOT NULL,
  `erstellungsDatum` date NOT NULL,
  `erstellungsZeit` time NOT NULL,
  `stundenTarifTag` double NOT NULL,
  `stundenTarifNacht` double NOT NULL,
  `kmTarifKlein` double NOT NULL,
  `kmTarifGross` double NOT NULL,
  `kategorieID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kategoriepreis`
--

INSERT INTO `kategoriepreis` (`kategoriePreisID`, `erstellungsDatum`, `erstellungsZeit`, `stundenTarifTag`, `stundenTarifNacht`, `kmTarifKlein`, `kmTarifGross`, `kategorieID`) VALUES
(1, '2018-12-03', '00:00:00', 2.7, 0.6, 0.5, 0.25, 1),
(2, '2018-12-03', '00:00:00', 2.7, 0.6, 0.54, 0.27, 2);

-- --------------------------------------------------------

--
-- Table structure for table `mahnung`
--

CREATE TABLE `mahnung` (
  `mahnungID` bigint(20) UNSIGNED NOT NULL,
  `druckDatum` date NOT NULL,
  `bezahlDatum` date DEFAULT NULL,
  `karrenzzeit` int(11) NOT NULL,
  `rechnungID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mahnung`
--

INSERT INTO `mahnung` (`mahnungID`, `druckDatum`, `bezahlDatum`, `karrenzzeit`, `rechnungID`) VALUES
(1, '2019-02-25', NULL, 10, 2),
(2, '2019-03-08', '2019-03-13', 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mietung`
--

CREATE TABLE `mietung` (
  `mietungID` bigint(20) UNSIGNED NOT NULL,
  `km` int(11) DEFAULT '0',
  `reservationID` bigint(20) UNSIGNED NOT NULL,
  `rechnungID` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mietung`
--

INSERT INTO `mietung` (`mietungID`, `km`, `reservationID`, `rechnungID`) VALUES
(3, 50, 1, 1),
(4, 250, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ort`
--

CREATE TABLE `ort` (
  `ortID` bigint(20) UNSIGNED NOT NULL,
  `ortschaft` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plz` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ort`
--

INSERT INTO `ort` (`ortID`, `ortschaft`, `plz`) VALUES
(1, 'Winterthur', 8400),
(2, 'Bern', 3000);

-- --------------------------------------------------------

--
-- Table structure for table `parkplatz`
--

CREATE TABLE `parkplatz` (
  `parkplatzID` bigint(20) UNSIGNED NOT NULL,
  `erstellungsDatum` date NOT NULL,
  `endDatum` date DEFAULT NULL,
  `strasse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nr` int(11) NOT NULL,
  `ortID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parkplatz`
--

INSERT INTO `parkplatz` (`parkplatzID`, `erstellungsDatum`, `endDatum`, `strasse`, `nr`, `ortID`) VALUES
(1, '2018-12-03', NULL, 'Bahnhofsstrasse', 5, 1),
(2, '2018-12-03', NULL, 'Bahnhofsstrasse', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `personID` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vorname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alter` int(11) NOT NULL,
  `strasse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `familienOberhaupt` bit(1) DEFAULT b'0',
  `gesperrt` bit(1) DEFAULT b'0',
  `mainPersonID` bigint(20) UNSIGNED DEFAULT NULL,
  `ortID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`personID`, `username`, `email`, `password`, `name`, `vorname`, `alter`, `strasse`, `familienOberhaupt`, `gesperrt`, `mainPersonID`, `ortID`) VALUES
(1, 'hans', 'hans.maier@gmx.ch', '5d41402abc4b2a76b9719d911017c592', 'Maier', 'Hans', 40, 'Bahnhofstr. 45', b'1', b'0', NULL, 1),
(2, 'petra123', 'petra.maier@google.com', '49f68a5c8493ec2c0bf489821c21fc3b', 'Maier', 'Petra', 42, 'Bahnhofstr. 45', b'0', b'0', 1, 1),
(3, 'klara.fest', 'klara.fest@carsharing.ch', '4fded1464736e77865df232cbcb4cd19', 'Fest', 'Klara', 34, 'Einfangstrasse 9', b'0', b'0', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `personenabo`
--

CREATE TABLE `personenabo` (
  `personenAboID` bigint(20) UNSIGNED NOT NULL,
  `art` enum('monatlich','jährlich') COLLATE utf8_unicode_ci NOT NULL,
  `erstellungsDatum` date NOT NULL,
  `erstellungsZeit` time NOT NULL,
  `aboID` bigint(20) UNSIGNED NOT NULL,
  `personID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `personenabo`
--

INSERT INTO `personenabo` (`personenAboID`, `art`, `erstellungsDatum`, `erstellungsZeit`, `aboID`, `personID`) VALUES
(1, 'monatlich', '2019-01-01', '19:00:00', 1, 1),
(2, 'monatlich', '2019-01-01', '16:00:00', 2, 1),
(3, 'monatlich', '2018-12-03', '10:00:00', 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `personrolle`
--

CREATE TABLE `personrolle` (
  `rolleID` bigint(20) UNSIGNED NOT NULL,
  `personID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `personrolle`
--

INSERT INTO `personrolle` (`rolleID`, `personID`) VALUES
(1, 3),
(2, 2),
(2, 3),
(3, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rechnung`
--

CREATE TABLE `rechnung` (
  `rechnungID` bigint(20) UNSIGNED NOT NULL,
  `preis` double NOT NULL,
  `druckDatum` date NOT NULL,
  `bezahlDatum` date DEFAULT NULL,
  `karrenzzeit` int(11) NOT NULL,
  `personID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rechnung`
--

INSERT INTO `rechnung` (`rechnungID`, `preis`, `druckDatum`, `bezahlDatum`, `karrenzzeit`, `personID`) VALUES
(1, 48.95, '2019-01-25', '2019-02-14', 30, 2),
(2, 105.55, '2019-01-25', NULL, 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `reservationID` bigint(20) UNSIGNED NOT NULL,
  `erstellungsDatum` date NOT NULL,
  `erstellungsZeit` time NOT NULL,
  `startDatum` date NOT NULL,
  `startZeit` time NOT NULL,
  `endDatum` date NOT NULL,
  `endZeit` time NOT NULL,
  `personID` bigint(20) UNSIGNED NOT NULL,
  `startParkplatzID` bigint(20) UNSIGNED NOT NULL,
  `endParkplatzID` bigint(20) UNSIGNED NOT NULL,
  `fahrzeugID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`reservationID`, `erstellungsDatum`, `erstellungsZeit`, `startDatum`, `startZeit`, `endDatum`, `endZeit`, `personID`, `startParkplatzID`, `endParkplatzID`, `fahrzeugID`) VALUES
(1, '2019-01-04', '14:00:00', '2019-01-04', '16:00:00', '2019-01-04', '22:00:00', 2, 1, 1, 1),
(2, '2019-01-07', '10:00:00', '2019-01-16', '12:00:00', '2019-01-17', '12:00:00', 1, 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rolle`
--

CREATE TABLE `rolle` (
  `rolleID` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rolle`
--

INSERT INTO `rolle` (`rolleID`, `name`) VALUES
(1, 'admin'),
(2, 'mitarbeiter'),
(3, 'benutzer');

-- --------------------------------------------------------

--
-- Table structure for table `verlängerung`
--

CREATE TABLE `verlängerung` (
  `verlängerungID` bigint(20) UNSIGNED NOT NULL,
  `erstellungsDatum` date NOT NULL,
  `erstellungsZeit` time NOT NULL,
  `endDatum` date NOT NULL,
  `endZeit` time NOT NULL,
  `reservationID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `verlängerung`
--

INSERT INTO `verlängerung` (`verlängerungID`, `erstellungsDatum`, `erstellungsZeit`, `endDatum`, `endZeit`, `reservationID`) VALUES
(1, '2019-01-04', '15:59:00', '2019-01-04', '17:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abo`
--
ALTER TABLE `abo`
  ADD PRIMARY KEY (`aboID`),
  ADD UNIQUE KEY `aboID` (`aboID`);

--
-- Indexes for table `ermässigung`
--
ALTER TABLE `ermässigung`
  ADD PRIMARY KEY (`ermässigungID`),
  ADD UNIQUE KEY `ermässigungID` (`ermässigungID`),
  ADD KEY `personID` (`personID`);

--
-- Indexes for table `fahrzeug`
--
ALTER TABLE `fahrzeug`
  ADD PRIMARY KEY (`fahrzeugID`),
  ADD UNIQUE KEY `fahrzeugID` (`fahrzeugID`),
  ADD KEY `parkplatzID` (`parkplatzID`),
  ADD KEY `fahrzeugTypeID` (`fahrzeugTypeID`);

--
-- Indexes for table `fahrzeugtype`
--
ALTER TABLE `fahrzeugtype`
  ADD PRIMARY KEY (`fahrzeugTypeID`),
  ADD UNIQUE KEY `fahrzeugTypeID` (`fahrzeugTypeID`),
  ADD KEY `kategorieID` (`kategorieID`);

--
-- Indexes for table `kategorie`
--
ALTER TABLE `kategorie`
  ADD PRIMARY KEY (`kategorieID`),
  ADD UNIQUE KEY `kategorieID` (`kategorieID`);

--
-- Indexes for table `kategoriepreis`
--
ALTER TABLE `kategoriepreis`
  ADD PRIMARY KEY (`kategoriePreisID`),
  ADD UNIQUE KEY `kategoriePreisID` (`kategoriePreisID`),
  ADD KEY `kategorieID` (`kategorieID`);

--
-- Indexes for table `mahnung`
--
ALTER TABLE `mahnung`
  ADD PRIMARY KEY (`mahnungID`),
  ADD UNIQUE KEY `mahnungID` (`mahnungID`),
  ADD KEY `rechnungID` (`rechnungID`);

--
-- Indexes for table `mietung`
--
ALTER TABLE `mietung`
  ADD PRIMARY KEY (`mietungID`),
  ADD UNIQUE KEY `mietungID` (`mietungID`),
  ADD KEY `rechnungID` (`rechnungID`),
  ADD KEY `reservationID` (`reservationID`);

--
-- Indexes for table `ort`
--
ALTER TABLE `ort`
  ADD PRIMARY KEY (`ortID`),
  ADD UNIQUE KEY `ortID` (`ortID`);

--
-- Indexes for table `parkplatz`
--
ALTER TABLE `parkplatz`
  ADD PRIMARY KEY (`parkplatzID`),
  ADD UNIQUE KEY `parkplatzID` (`parkplatzID`),
  ADD KEY `ortID` (`ortID`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`personID`),
  ADD UNIQUE KEY `personID` (`personID`),
  ADD KEY `mainPersonID` (`mainPersonID`),
  ADD KEY `ortID` (`ortID`);

--
-- Indexes for table `personenabo`
--
ALTER TABLE `personenabo`
  ADD PRIMARY KEY (`personenAboID`),
  ADD UNIQUE KEY `personenAboID` (`personenAboID`),
  ADD KEY `personID` (`personID`),
  ADD KEY `aboID` (`aboID`);

--
-- Indexes for table `personrolle`
--
ALTER TABLE `personrolle`
  ADD PRIMARY KEY (`rolleID`,`personID`),
  ADD KEY `personID` (`personID`);

--
-- Indexes for table `rechnung`
--
ALTER TABLE `rechnung`
  ADD PRIMARY KEY (`rechnungID`),
  ADD UNIQUE KEY `rechnungID` (`rechnungID`),
  ADD KEY `personID` (`personID`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`reservationID`),
  ADD UNIQUE KEY `reservationID` (`reservationID`),
  ADD KEY `personID` (`personID`),
  ADD KEY `startParkplatzID` (`startParkplatzID`),
  ADD KEY `endParkplatzID` (`endParkplatzID`),
  ADD KEY `fahrzeugID` (`fahrzeugID`);

--
-- Indexes for table `rolle`
--
ALTER TABLE `rolle`
  ADD PRIMARY KEY (`rolleID`),
  ADD UNIQUE KEY `rolleID` (`rolleID`);

--
-- Indexes for table `verlängerung`
--
ALTER TABLE `verlängerung`
  ADD PRIMARY KEY (`verlängerungID`),
  ADD UNIQUE KEY `verlängerungID` (`verlängerungID`),
  ADD KEY `reservationID` (`reservationID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abo`
--
ALTER TABLE `abo`
  MODIFY `aboID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ermässigung`
--
ALTER TABLE `ermässigung`
  MODIFY `ermässigungID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fahrzeug`
--
ALTER TABLE `fahrzeug`
  MODIFY `fahrzeugID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fahrzeugtype`
--
ALTER TABLE `fahrzeugtype`
  MODIFY `fahrzeugTypeID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kategorie`
--
ALTER TABLE `kategorie`
  MODIFY `kategorieID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kategoriepreis`
--
ALTER TABLE `kategoriepreis`
  MODIFY `kategoriePreisID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mahnung`
--
ALTER TABLE `mahnung`
  MODIFY `mahnungID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mietung`
--
ALTER TABLE `mietung`
  MODIFY `mietungID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ort`
--
ALTER TABLE `ort`
  MODIFY `ortID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `parkplatz`
--
ALTER TABLE `parkplatz`
  MODIFY `parkplatzID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `personID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personenabo`
--
ALTER TABLE `personenabo`
  MODIFY `personenAboID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rechnung`
--
ALTER TABLE `rechnung`
  MODIFY `rechnungID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `reservationID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rolle`
--
ALTER TABLE `rolle`
  MODIFY `rolleID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `verlängerung`
--
ALTER TABLE `verlängerung`
  MODIFY `verlängerungID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ermässigung`
--
ALTER TABLE `ermässigung`
  ADD CONSTRAINT `ermässigung_ibfk_1` FOREIGN KEY (`personID`) REFERENCES `person` (`personID`);

--
-- Constraints for table `fahrzeug`
--
ALTER TABLE `fahrzeug`
  ADD CONSTRAINT `fahrzeug_ibfk_1` FOREIGN KEY (`parkplatzID`) REFERENCES `parkplatz` (`parkplatzID`),
  ADD CONSTRAINT `fahrzeug_ibfk_2` FOREIGN KEY (`fahrzeugTypeID`) REFERENCES `fahrzeugtype` (`fahrzeugTypeID`);

--
-- Constraints for table `fahrzeugtype`
--
ALTER TABLE `fahrzeugtype`
  ADD CONSTRAINT `fahrzeugtype_ibfk_1` FOREIGN KEY (`kategorieID`) REFERENCES `kategorie` (`kategorieID`);

--
-- Constraints for table `kategoriepreis`
--
ALTER TABLE `kategoriepreis`
  ADD CONSTRAINT `kategoriepreis_ibfk_1` FOREIGN KEY (`kategorieID`) REFERENCES `kategorie` (`kategorieID`);

--
-- Constraints for table `mahnung`
--
ALTER TABLE `mahnung`
  ADD CONSTRAINT `mahnung_ibfk_1` FOREIGN KEY (`rechnungID`) REFERENCES `rechnung` (`rechnungID`);

--
-- Constraints for table `mietung`
--
ALTER TABLE `mietung`
  ADD CONSTRAINT `mietung_ibfk_1` FOREIGN KEY (`rechnungID`) REFERENCES `rechnung` (`rechnungID`),
  ADD CONSTRAINT `mietung_ibfk_2` FOREIGN KEY (`reservationID`) REFERENCES `reservation` (`reservationID`);

--
-- Constraints for table `parkplatz`
--
ALTER TABLE `parkplatz`
  ADD CONSTRAINT `parkplatz_ibfk_1` FOREIGN KEY (`ortID`) REFERENCES `ort` (`ortID`);

--
-- Constraints for table `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `person_ibfk_1` FOREIGN KEY (`mainPersonID`) REFERENCES `person` (`personID`),
  ADD CONSTRAINT `person_ibfk_2` FOREIGN KEY (`ortID`) REFERENCES `ort` (`ortID`);

--
-- Constraints for table `personenabo`
--
ALTER TABLE `personenabo`
  ADD CONSTRAINT `personenabo_ibfk_1` FOREIGN KEY (`personID`) REFERENCES `person` (`personID`),
  ADD CONSTRAINT `personenabo_ibfk_2` FOREIGN KEY (`aboID`) REFERENCES `abo` (`aboID`);

--
-- Constraints for table `personrolle`
--
ALTER TABLE `personrolle`
  ADD CONSTRAINT `personrolle_ibfk_1` FOREIGN KEY (`personID`) REFERENCES `person` (`personID`),
  ADD CONSTRAINT `personrolle_ibfk_2` FOREIGN KEY (`rolleID`) REFERENCES `rolle` (`rolleID`);

--
-- Constraints for table `rechnung`
--
ALTER TABLE `rechnung`
  ADD CONSTRAINT `rechnung_ibfk_1` FOREIGN KEY (`personID`) REFERENCES `person` (`personID`);

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`personID`) REFERENCES `person` (`personID`),
  ADD CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`startParkplatzID`) REFERENCES `parkplatz` (`parkplatzID`),
  ADD CONSTRAINT `reservation_ibfk_3` FOREIGN KEY (`endParkplatzID`) REFERENCES `parkplatz` (`parkplatzID`),
  ADD CONSTRAINT `reservation_ibfk_4` FOREIGN KEY (`fahrzeugID`) REFERENCES `fahrzeug` (`fahrzeugID`);

--
-- Constraints for table `verlängerung`
--
ALTER TABLE `verlängerung`
  ADD CONSTRAINT `verlängerung_ibfk_1` FOREIGN KEY (`reservationID`) REFERENCES `reservation` (`reservationID`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
