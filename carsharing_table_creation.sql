# Database created on 14/01/19 1024 from /home/sebi/Documents/school/berufschule/m153/carsharing/carsharing/carsharing - physikalisches diagram.wsd
DROP DATABASE IF EXISTS carsharing;
CREATE DATABASE carsharing CHARACTER SET = utf8 COLLATE = utf8_unicode_ci;
USE carsharing;

CREATE TABLE IF NOT EXISTS `abo` (
  aboID            SERIAL,
  name            varchar(255) NOT NULL,
  preis           DOUBLE NOT NULL,
  erstellungsDatum DATE NOT NULL,
  erstellungsZeit TIME NOT NULL,
  relatedAboID BIGINT UNSIGNED,
  PRIMARY KEY (aboID));

CREATE TABLE IF NOT EXISTS `personenabo` (
  personenAboID    SERIAL,
  art             ENUM('monatlich', 'jährlich') NOT NULL,
  erstellungsDatum DATE NOT NULL,
  erstellungsZeit TIME NOT NULL,
  aboID  BIGINT UNSIGNED NOT NULL,
  personID        BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (personenAboID));

CREATE TABLE IF NOT EXISTS `rolle` (
  rolleID          SERIAL,
  name            varchar(255) NOT NULL,
  PRIMARY KEY (rolleID));

CREATE TABLE IF NOT EXISTS `personrolle` (
  rolleID          BIGINT UNSIGNED NOT NULL,
  personID        BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (rolleID, personID));

CREATE TABLE IF NOT EXISTS `person` (
  personID         SERIAL,
  username        varchar(255) NOT NULL,
  email           varchar(255) NOT NULL,
  password        varchar(255) NOT NULL,
  name            varchar(255) NOT NULL,
  vorname         varchar(255) NOT NULL,
  `alter`           INT NOT NULL,
  strasse         varchar(255) NOT NULL,
  familienOberhaupt BIT DEFAULT 0,
  gesperrt        BIT DEFAULT 0,
  mainPersonID       BIGINT UNSIGNED,
  ortID           BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (personID));

CREATE TABLE IF NOT EXISTS `ort` (
  ortID            SERIAL,
  ortschaft       varchar(255) NOT NULL,
  plz             INT NOT NULL,
  PRIMARY KEY (ortID));

CREATE TABLE IF NOT EXISTS `ermässigung` (
  ermässigungID      SERIAL,
  name            varchar(255) NOT NULL,
  startdatum      DATE NOT NULL,
  endDatum        DATE,
  reduktion       DOUBLE NOT NULL,
  personID        BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (ermässigungID));

CREATE TABLE IF NOT EXISTS `reservation` (
  reservationID    SERIAL,
  erstellungsDatum DATE NOT NULL,
  erstellungsZeit TIME NOT NULL,
  startDatum      DATE NOT NULL,
  startZeit       TIME NOT NULL,
  endDatum        DATE NOT NULL,
  endZeit         TIME NOT NULL,
  personID        BIGINT UNSIGNED NOT NULL,
  startParkplatzID BIGINT UNSIGNED NOT NULL,
  endParkplatzID  BIGINT UNSIGNED NOT NULL,
  fahrzeugID      BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (reservationID));

CREATE TABLE IF NOT EXISTS `verlängerung` (
  verlängerungID   SERIAL,
  erstellungsDatum DATE NOT NULL,
  erstellungsZeit TIME NOT NULL,
  endDatum        DATE NOT NULL,
  endZeit         TIME NOT NULL,
  reservationID   BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (verlängerungID));

CREATE TABLE IF NOT EXISTS `mietung` (
  mietungID          SERIAL,
  km              INT DEFAULT 0,
  reservationID   BIGINT UNSIGNED NOT NULL,
  rechnungID      BIGINT UNSIGNED,
  PRIMARY KEY (mietungID));

CREATE TABLE IF NOT EXISTS `rechnung` (
  rechnungID        SERIAL,
  preis           DOUBLE NOT NULL,
  druckDatum      DATE NOT NULL,
  bezahlDatum     DATE,
  karrenzzeit     INT NOT NULL,
  personID        BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (rechnungID));

CREATE TABLE IF NOT EXISTS `mahnung` (
  mahnungID        SERIAL,
  druckDatum      DATE NOT NULL,
  bezahlDatum     DATE,
  karrenzzeit     INT NOT NULL,
  rechnungID      BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (mahnungID));

CREATE TABLE IF NOT EXISTS `fahrzeug` (
  fahrzeugID       SERIAL,
  jahrgang        INT NOT NULL,
  gefahreneKm     INT DEFAULT 0,
  benzinstand     INT DEFAULT 100,
  zustand         INT DEFAULT 5,
  bemerkung       TEXT,
  parkplatzID     BIGINT UNSIGNED NOT NULL,
  fahrzeugTypeID  BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (fahrzeugID));

CREATE TABLE IF NOT EXISTS `fahrzeugtype` (
  fahrzeugTypeID   SERIAL,
  marke           varchar(255) NOT NULL,
  model           varchar(255) NOT NULL,
  beschreibung    TEXT,
  maxPersonen     INT NOT NULL,
  maxGepäck       INT NOT NULL,
  schaltung       INT NOT NULL,
  imageUUID       varchar(255),
  kategorieID     BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (fahrzeugTypeID));

CREATE TABLE IF NOT EXISTS `kategorie` (
  kategorieID      SERIAL,
  name            varchar(255) NOT NULL,
  PRIMARY KEY (kategorieID));

CREATE TABLE IF NOT EXISTS `kategoriepreis` (
  kategoriePreisID   SERIAL,
  erstellungsDatum DATE NOT NULL,
  erstellungsZeit TIME NOT NULL,
  stundenTarifTag DOUBLE NOT NULL,
  stundenTarifNacht DOUBLE NOT NULL,
  kmTarifKlein    DOUBLE NOT NULL,
  kmTarifGross    DOUBLE NOT NULL, kategorieID     BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (kategoriePreisID));

CREATE TABLE IF NOT EXISTS `parkplatz` (
  parkplatzID      SERIAL,
  erstellungsDatum DATE NOT NULL,
  endDatum        DATE,
  strasse         varchar(255) NOT NULL,
  nr              INT NOT NULL,
  ortID           BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (parkplatzID));


alter table personenabo
add foreign key (personID) references person(personID),
add foreign key (aboID) references abo(aboID);

alter table person
add foreign key (mainPersonID) references person(personID),
add foreign key (ortID) references ort(ortID);

alter table personrolle 
add foreign key(personID) references person(personID),
add foreign key(rolleID) references rolle(rolleID);

alter table ermässigung
add foreign key (personID) references person(personID);

alter table reservation
add foreign key (personID) references person(personID),
add foreign key(startParkplatzID) references parkplatz(parkplatzID),
add foreign key(endParkplatzID) references parkplatz(parkplatzID),
add foreign key(fahrzeugID) references fahrzeug(fahrzeugID);

alter table parkplatz
add foreign key (ortID) references ort(ortID);

alter table verlängerung
add foreign key (reservationID) references reservation(reservationID);

alter table fahrzeug
add foreign key (parkplatzID) references parkplatz(parkplatzID),
add foreign key (fahrzeugTypeID) references fahrzeugtype(fahrzeugTypeID);

alter table fahrzeugtype 
add foreign key (kategorieID) references kategorie(kategorieID);

alter table kategoriepreis
add foreign key (kategorieID) references kategorie(kategorieID);

alter table mietung 
add foreign key (rechnungID) references rechnung(rechnungID),
add foreign key (reservationID) references reservation(reservationID);

alter table rechnung
add foreign key (personID) references person(personID);

alter table mahnung
add foreign key (rechnungID) references rechnung(rechnungID);